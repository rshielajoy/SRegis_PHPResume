<html>

    <header>
        <title> RESUME </title>
        <link href = "styles.css" rel = "stylesheet" type = "text/css">
    </header>

    <body>
        <div class = "user_container">
            <img src = "user2.png" alt = "user_pic" class = "r_user"/> </br>
            <h1 id = "r_name"> SHIELA JOY REGIS </h1>

            <div class = "profile"> 
                <h3>  PROFILE </h3>
                    <?php echo 
                        " <p id = 'profile'> 
                            I am Shiela, has a strong sense of professionalism
                            in generating programs. To seek and obtain for a dynamic and challenging opportunity that
                            contributes to the outstanding success of the IT world.
                        </p>"
                    ?>
            </div>

            <div class = "contacts">
                <h3 id = "contacts"> CONTACTS </h3>
                
                <img src = "address.png" alt = "" class = "address"/>
                <?php echo "<p id = 'address'> NHA Bangkal Davao City </p>" ?>

                <img src = "tphone.png" alt = "" class = "phone"/>
                <?php echo "<p id = 'phone'> 09** *** **** </p>" ?>

                <img src = "emailb.png" alt = "" class = "email"/>
                <?php echo "<p id = 'email'> regisshielajoy@gmail.com </p>" ?>
            </div>

            
            <div class = "skills">
                <h3 id = "skills"> SKILLS </h3>
                <img src = "html.png" alt = "html" 
                    id = "html" width = "70" height = "70" />
                <img src = "css.png" alt = "css" 
                id = "css" width = "80" height = "80" />
                <img src = "prog.png" alt = "programming" 
                id = "programming" width = "80" height = "73" />
                <img src = "photoshop.png" alt = "photoshop" 
                id = "photoshop" width = "80" height = "80" />
            </div>
        </div>

        <div class = "sideB">
            <div class = "imagebanner">
                <img src = "banner.jpg" alt = "banner" 
                    id = "iBanner" height = "280" width = "750"/>
                <?php echo "<div class = 'textbanner'> Create. Style. Manage. </div>" ?>
                <div class = "sendme"> 
                    <nav>
                        <ul>
                            <li> <a href = "contact.php"> CONTACT ME </a> </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class = "sideInfo"> 
                <?php echo "<h2 id = 'personinfo'> PERSONAL INFORMATION: </h2>" ?>
                <?php echo "
                    <div class = 'infcontent'>
                        <p> <b> Age: </b> 19 years old </p>
                        <p> <b> Gender: </b> Female  </p>
                        <p> <b> Status: </b> Single </p>
                        <p> <b> Birth Date: </b>  October 07, 1998 </p>
                        <p> <b> Nationality: </b>  Filipino </p>
                    </div>"
                ?>

                <?php echo "<h2 id = 'education'> EDUCATION: </h2>" ?>

                <?php echo "
                    <div class = 'educ_content'>
                        <img src = 'ktmsces.png' 
                            alt = 'ktmsces' id = 'picEdu1' width =  '80' height = '70'/>
                        <p id = 'eKtmsces'> 
                            <b> Kapitan Tomas Monteverde Sr. Central Elementary School (2005 - 2011) </b> </br>
                            <i> C. Bangoy St., Ponciano Davao City </i>  
                        </p>

                        <img src = 'dcnhs.png'
                        alt = 'dcnhs' id = 'picEdu2' width = '80' height = '70'/>
                        <p id = 'eDcnhs'> 
                            <b> Davao City National High School (2011 - 2015) </b> </br>
                            <i> F. Torres St., Davao City </i> 
                        </p>

                        <img src = 'usep.png'
                        alt = 'usep' id = 'picEdu3' width = '80' height = '70'/>
                        <p id = 'eUsep'> 
                                <b> University of Southeastern Philippines (2015 - Present) </b> </br>
                                <b style = 'color:red'> Bachelor of Science in Information Technology </b> </br>
                                <i> B.O. St., JP Laurel, Davao City </i> 
                            </p>
                    </div>"
                ?>
        </div>
    </body>

    <footer class = "RFooter">
        <?php echo "<p>  &copy; Copyrights Reserved 2018 </p>" ?>
    </footer>
</html>
